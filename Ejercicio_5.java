//Aquí te presento la clase Película con sus atributos privados, métodos get/set y un método mostrarDatos() para mostrar la información de la película. También he creado una clase Main donde se instancian 
//dos objetos de la clase Película y se muestran sus datos.
public class Ejercicio_5 {
    private String título;
    private String género;
    private int duración;
    private String calificación;
    private String director;
    
public Ejercicio_5(String título, String género, int duración, String calificación, String director) {

        this.título = título;
        this.género = género;
        this.duración = duración;
        this.calificación = calificación;
        this.director = director;
    }
    
public String getTítulo() {
        return título;
    }
    
 public void setTítulo(String título) {
        this.título = título;
    }
    
public String getGénero() {
        return género;
    }
    
 public void setGénero(String género) {
        this.género = género;
    }
    
public int getDuración() {
        return duración;
    }
    
 public void setDuración(int duración) {
        this.duración = duración;
    }
    
public String getCalificación() {
        return calificación;
    }
    
 public void setCalificación(String calificación) {
        this.calificación = calificación;
    }
    
public String getDirector() {
        return director;
    }
    
 public void setDirector(String director) {
        this.director = director;
    }
    
public void mostrarDatos() {
    System.out.println("Película: " + título);
    System.out.println("Género: " + género);
    System.out.println("Duración: " + duración + " minutos");
    System.out.println("Calificación: " + calificación);
    System.out.println("Director: " + director);
    }


public static void main(String[] args) {
    Ejercicio_5 película1 = new Ejercicio_5("Hobbit 1", " Aventura/Fantasia", 169, " No recomendado a menores de 13 años", "Peter Jackson ");
    Ejercicio_5 película2 = new Ejercicio_5("Star Wars: Episodio V - El Imperio contraataca", "Ciencia ficción; Épico", 124, "Apta para todo público", "Irvin Kershner");
        
    System.out.println("Película 1:");
    película1.mostrarDatos();
    System.out.println();
        
    System.out.println("Película 2:");
    película2.mostrarDatos();
    System.out.println();
    }
}



