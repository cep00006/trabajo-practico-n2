 public class Ejercicio_2 {
    //Declaro atributos 
    private float lado1;
    private float lado2;
    private float lado3;

    //creo metodo    
public Ejercicio_2 (float lado1,float lado2, float lado3) {
    
     this.lado1 = lado1;
     this.lado2 = lado2;
     this.lado3 = lado3;
    
        }
 
 // creo metodo para comprobar que tipo de triangulo es
public  void comprobar(){
    
 // Verifico si el triángulo es equilátero
    if (lado1 == lado2 && lado2 == lado3) {
    System.out.println("El triángulo es EQUILATERO ya que sus lados son iguales:  " +  (lado1)+"    "+ (lado2)+ "   " + (lado3));
        }
 // Verifico si el triángulo es isósceles
    else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
    System.out.println("El triángulo es ISOSCELES ya que tiene dos  lados son iguales:  " +  (lado1)+"    "+ (lado2)+ "   " + (lado3));
               
        }
// Si no es equilátero ni isósceles, entonces es escaleno
    else {
    System.out.println("El triángulo es ESCALENO no tiene lados iguales."+  (lado1)+"    "+ (lado2)+ "   " + (lado3));
        }
    
    }
        
// creo la funcion main para ejecutar y llamo el metodo     
   public static void main(String [] arg ){
   
//Creacion de un objeto  y le asigno los valores
       
     Ejercicio_2 trianguloequilatero = new Ejercicio_2 (8,8,8);  
   
     trianguloequilatero.comprobar();

//Creacion de otro objeto y asigno valores

      Ejercicio_2 triangulisoceles = new Ejercicio_2 (5,10,10);  
   
      triangulisoceles.comprobar();

//Creacion de otro objeto y le asigno valores
         
     Ejercicio_2 trianguloescaleno = new Ejercicio_2 (3,6,9);  
   
     trianguloescaleno.comprobar();
   
    }
  

}



