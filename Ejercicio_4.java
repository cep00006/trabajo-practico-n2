public class Ejercicio_4 {

//defino  un atributo privado int array    

    private int[] numeros;
    

public Ejercicio_4(int[] numeros) {

//creo un constructor para que reciba un array entero    

        this.numeros = numeros;
    }

// creo un metodo para que me sume numeros pares

public int sumaPares() {

    int suma = 0;

        for (int numero : numeros) {
            if (numero % 2 == 0) {
                suma += numero;
            }
        }
        return suma;
    }

// creo un metodo para que me de la cantidad de pares 

    public int cantidadPares() {
        int cantidad = 0;
        for (int numero : numeros) {
            if (numero % 2 == 0) {
                cantidad++;
            }
        }
        return cantidad;
    }

// creo un metodo para que me sume los impares 

public int sumaImpares() {
    
    int suma = 0;
        for (int numero : numeros) {
            if (numero % 2 != 0) {
                suma += numero;
            }
        }
        return suma;
    }

// // creo un metodo para que me la cantidad de impares

public int cantidadImpares() {
    
    int cantidad = 0;
        for (int numero : numeros) {
            if (numero % 2 != 0) {
                cantidad++;
            }
        }
        return cantidad;
    }


// creo un metodo para que me el maximo de pares 

public int maximoPares() {
    
    int maximo = Integer.MIN_VALUE;
        for (int numero : numeros) {
            if (numero % 2 == 0 && numero > maximo) {
                maximo = numero;
            }
        }
        return maximo;
    }

// creo un metodo para que me de el minimo 

public int minimoImpares() {
    
    int minimo = Integer.MAX_VALUE;
        for (int numero : numeros) {
            if (numero % 2 != 0 && numero < minimo) {
                minimo = numero;
            }
        }
        return minimo;
    }

    
// creo la funcion main  lleno el vector y llamo los imprimir los metodos
public static void main(String[] args) {
    
    int[] numeros = {10, 33, 5, 7, 9, 2, 8, 16, 32, 90};

    Ejercicio_4 objeto1ArrayNumeros = new Ejercicio_4(numeros);
    Ejercicio_4 objeto2ArrayNumeros = new Ejercicio_4(new int[] {4, 6, 8, 10, 12,5,7,4,0,50});

    System.out.println("objeto1:"); 

    System.out.println("Suma de los elementos pares: " + objeto1ArrayNumeros.sumaPares());
    System.out.println("Cantidad de elementos pares: " + objeto1ArrayNumeros.cantidadPares());
    System.out.println("Suma de los elementos impares: " + objeto1ArrayNumeros.sumaImpares());
    System.out.println("Cantidad de elementos impares: " + objeto1ArrayNumeros.cantidadImpares());
    System.out.println("Valor máximo de los elementos pares: " + objeto1ArrayNumeros.maximoPares());
    System.out.println("Valor mínimo de los elementos impares: " + objeto1ArrayNumeros.minimoImpares());
    
    System.out.println("objeto2:");

    System.out.println("Suma de los elementos pares: " + objeto2ArrayNumeros.sumaPares());
    System.out.println("Cantidad de elementos pares: " + objeto2ArrayNumeros.cantidadPares());
    System.out.println("Suma de los elementos impares: " + objeto2ArrayNumeros.sumaImpares());
    System.out.println("Cantidad de elementos impares: " + objeto2ArrayNumeros.cantidadImpares());
    System.out.println("Valor máximo de los elementos pares: " + objeto2ArrayNumeros.maximoPares());
    System.out.println("Valor mínimo de los elementos impares: " + objeto2ArrayNumeros.minimoImpares());

    }
  
     
}
