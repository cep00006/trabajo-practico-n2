public class Ejercicio_1 {
//defino  un atributo privado cadena para almacenar en cadena los caracteres
   
    private String cadena;

//creo  un constructor que toma una cadena de caracteres  y la guarda en el atributo cadena.

public Ejercicio_1(String cadena) {
  
    this.cadena = cadena;
    
    }
    
//creo un  método para que muestra la primera mitad de los  caracteres de la cadena
// se utiliza  una  división entera para  y con  el método substring() se obtiene la primera mitad de la cadena.
    
public void mostrarPrimeraMitad() {

    int mitad = this.cadena.length() / 2;
    String primeraMitad = this.cadena.substring(0, mitad);
    System.out.println(primeraMitad);
     
     }
      
// creo un metodo para mostrar la cadena en mayuscula y con toUpperCase() convierto los caracteres a mayúsculas

public void mostrarEnMayuscula() {
           
    String enMayuscula = this.cadena.toUpperCase();
           
    System.out.println(enMayuscula); 

     }

//creo un método para mostra el ultimo caracter, y con charAt() y se le pasa como argumento el índice correspondiente al último
// carácter de la cadena que es la longitud de la cadena menos 1.

public void mostrarUltimoCaracter() {
            
    char ultimoCaracter = this.cadena.charAt(this.cadena.length() - 1);
    
    System.out.println(ultimoCaracter);
    
    }

//se cre un metodo para mostrar el texto en forma inversa, con un bucle for que comienza 
//desde la posición del último carácter y avanza hacia atrás, guardando  cada carácter en una variable inversa.

public void mostrarInversa() {
           
    String inversa = "";
         
    for (int i = this.cadena.length() - 1; i >= 0; i--) {
       
        inversa += this.cadena.charAt(i);
         
         }
            
         System.out.println(inversa);
        
    }

//cre un método para  mostrar con guiones, se utiliza un bucle for para  iterar a través de cada
//carácter y los guarda en la variable cadenaConGuiones, agregando un guión después de cada carácter

 public void mostrarConGuiones() {
    String cadenaConGuiones = "";
       
         for (int i = 0; i < this.cadena.length(); i++) {
         
            cadenaConGuiones += this.cadena.charAt(i);
        
             if (i != this.cadena.length() - 1) {
             
                cadenaConGuiones += "-";
            }
        
        }
   
        System.out.println(cadenaConGuiones);
    }

//creo un metodo contar vocales,se utiliza un bucle for para  iterar  a través de cada carácter
// y se verifica con un if si es una vocal (a, e, i, o o u). Si es una vocal, se incrementa el contador

public int contarVocales() {
    int contador = 0;
    
         for (int i = 0; i < this.cadena.length(); i++) {
         char c = this.cadena.charAt(i);
       
             if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
             contador++;
             }
         }
         return contador;
}

 // creo la funcion main y luego llamo a los metodos creados

 public static void main(String[] args) {

    String cadena = "ejercios de trabajo practico n2";
    Ejercicio_1 objeto1 = new Ejercicio_1(cadena);
    objeto1.mostrarPrimeraMitad();

     //objeto 2
    
     Ejercicio_1 objeto2 = new Ejercicio_1(cadena);
     objeto2.mostrarEnMayuscula();
     
     //objeto 3

     Ejercicio_1 objeto3 = new Ejercicio_1(cadena);
     objeto3.mostrarUltimoCaracter();

     //objeto 4
      
     Ejercicio_1 objeto4 = new Ejercicio_1(cadena);
     objeto4.mostrarInversa();

     //objeto 5
   
     Ejercicio_1 objeto5 = new Ejercicio_1(cadena);
     objeto5.mostrarConGuiones();

     // objeto 6

     Ejercicio_1 objeto6= new Ejercicio_1(cadena);
     int cantidadVocales  = objeto6.contarVocales();
     System.out.println("La cadena tiene " + cantidadVocales  + " vocales.");

    }
  
    
}























   
