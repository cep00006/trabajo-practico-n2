public class Ejercicio_3 {
  
  // Declaro constantes con valores que no cambian 
  public static final int EDAD_MINIMA = 18;
  public static final int EDAD_MAXIMA = 120;

  // Creo un método que verifica si la edad de la persona está en el rango permitido para votar
  //en una variable de tipo boolean y con un if controlo si esta habilitado
 
  public static void edadParaVotar(int edad) {

    boolean variableParaVotar = (edad >= EDAD_MINIMA && edad <= EDAD_MAXIMA);
 
       if (variableParaVotar) {
        System.out.println("Votante habilitado");
       } else {
          System.out.println("Votante inhabilitado");
      }
   }

  public static void main(String[] args) {

    // Llamo al método con 2 edades
      
    System.out.println("   ");
    
    edadParaVotar(38);
    
    edadParaVotar(15);
  
  }
}
