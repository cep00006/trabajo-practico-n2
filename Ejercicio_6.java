public class Ejercicio_6 {
    
    private int codigo;
    private String nombre;
    private double precioCosto;
    private double porcentajeGanancia;
    private final double iva = 0.21;
    private double precioVenta;
    
 // Constructor
public Ejercicio_6(int codigo, String nombre, double precioCosto, double porcentajeGanancia) {

        this.codigo = codigo;
        this.nombre = nombre;
        this.precioCosto = precioCosto;
        this.porcentajeGanancia = porcentajeGanancia;
        this.precioVenta = calcularPrecioVenta();
    }
    
// Getter y setter para el código
public int getCodigo() {
        return codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
// Getter y setter para el nombre
public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
// Getter y setter para el precio de costo
public double getPrecioCosto() {
        return precioCosto;
    }
    
    public void setPrecioCosto(double precioCosto) {
        this.precioCosto = precioCosto;
    }
    
// Getter y setter para el porcentaje de ganancia
 public double getPorcentajeGanancia() {
        return porcentajeGanancia;
    }
    
    public void setPorcentajeGanancia(double porcentajeGanancia) {
        this.porcentajeGanancia = porcentajeGanancia;
        this.precioVenta = calcularPrecioVenta();
    }
    
// Método privado para calcular el precio de venta
private double calcularPrecioVenta() {

     double precioSinIVA = precioCosto * (1 + porcentajeGanancia);
     double precioConIVA = precioSinIVA * (1 + iva);
        return precioConIVA;
    }
    
// Getter para el precio de venta
public double getPrecioVenta() {
        return precioVenta;
    }
    
// Función main para demostrar el uso de la clase Producto

public static void main(String[] args) {
    // Crear un objeto Producto
    Ejercicio_6 producto1 = new Ejercicio_6(123, "Televisor Plasma", 8900.55, 0.2);
    Ejercicio_6 producto2 = new Ejercicio_6(456, "Equipo de Musica", 8900.55, 0.2);
       
    // Imprimir los valores iniciales
    
    System.out.println("Producto1:"); 


    System.out.println("Código: " + producto1.getCodigo());
    System.out.println("Nombre: " + producto1.getNombre());
    System.out.println("Precio de costo:$ " + producto1.getPrecioCosto());
    System.out.println("Porcentaje de ganancia: " + producto1.getPorcentajeGanancia()+" % ");
    System.out.println("Precio de venta:$ " + producto1.getPrecioVenta());

    System.out.println("Producto2:"); 
    
    System.out.println("Código: " + producto2.getCodigo());
    System.out.println("Nombre: " + producto2.getNombre());
    System.out.println("Precio de costo:$ " + producto2.getPrecioCosto());
    System.out.println("Porcentaje de ganancia: " + producto2.getPorcentajeGanancia()+" % ");
    System.out.println("Precio de venta:$ " + producto2.getPrecioVenta());
       
    
    }

}
